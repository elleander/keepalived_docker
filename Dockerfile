FROM centos:7
RUN yum -y update && \
    yum -y install keepalived && \
    yum clean all && \
    rm -rf /var/cache/yum/
CMD ["keepalived", "-n", "-P", "-l", "-f", "/etc/keepalived/keepalived.cfg", "-D"]
